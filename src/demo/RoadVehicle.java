package demo;

public abstract class RoadVehicle implements Vehicle {

	private String colour;
	
	// this is a concrete method because it has an implementation
	public String getColour() {
		return colour;
	}
	
	// this method has no implementation and so must be defined as abstract
	// classes that extend the Vehicle class must implement it
	// This allows us to have manual ("stickshift") or automatic vehicles
	public abstract void changeGear();
	
}
