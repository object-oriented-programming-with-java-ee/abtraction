package demo;

public interface Vehicle {

	// All vehicles have a colour
	public String getColour(); 
	
}
